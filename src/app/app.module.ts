import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PaymentInfo } from '../pages/paymentinfo/paymentinfo';
import { LumpSum } from '../pages/lumpsum/lumpsum';
import { Affordability } from '../pages/affordability/affordability';
import { FutureValue } from '../pages/futurevalue/futurevalue';
import {PresentValue } from '../pages/presentvalue/presentvalue';
import { LoanPage } from '../pages/loan/loan';
import {SettingsPage } from '../pages/settings/settings';
import { IonicStorageModule } from '@ionic/storage';
import { ResultsPage } from '../pages/results/results';
import { Results2Page } from '../pages/results2/results2';
import { Results3Page } from '../pages/results3/results3';
import {  HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PaymentInfo,
    LumpSum,
    Affordability,
    FutureValue,
    PresentValue,
    LoanPage,
    SettingsPage,
    ResultsPage,
    Results2Page,
    Results3Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PaymentInfo,
    LumpSum,
    Affordability,
    FutureValue,
    PresentValue,
    LoanPage,
    SettingsPage,
    ResultsPage,
    Results2Page,
    Results3Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
    
  ]
})
export class AppModule {}
