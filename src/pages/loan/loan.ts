import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage'

/**
 * Generated class for the LoanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loan',
  templateUrl: 'loan.html',
})
export class LoanPage {
public Amount: number;
public Rate:number;
public YearsPay: number;
public PayPeriod: string;
public results: number;
public LoanPayment: number;
public NumPayment: number;
public Interest: any;
public Total: number;
public CurrentTotal: number;
public Count: number;
public PeriodicRate: number;
public Period: number;
 
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanPage');

  }

  ionViewDidEnter(){
    this.storage.get('Amount').then(data =>{this.Amount = data;})
    this.storage.get('Rate').then(data =>{this.Rate = data;})
    this.storage.get('YearsPay').then(data =>{this.YearsPay = data;})
    this.storage.get('PayPeriod').then(data =>{this.PayPeriod = data;})
    this.storage.get('LoanPayment').then(data =>{this.LoanPayment = data})
    this.storage.get('NumPayment').then(data =>{this.NumPayment = data})
    this.storage.get('Interest').then(data =>{this.Interest = data})
    this.storage.get('Total').then(data =>{this.Total = data})
  }

  ionViewCanLeave(){
    this.storage.set('Amount', this.Amount);
    this.storage.set('Rate', this.Rate);
    this.storage.set('YearsPay', this.YearsPay);
    this.storage.set('PayPeriod', this.PayPeriod);
  }

  calcLoanPayment(periodicRate, periods, presentValue){
    if (periodicRate === 0){
      return -presentValue / periods;
    }
    var r = ((1 + +periodicRate) ** periods);
    return ((presentValue * r)/ (r - 1)) * periodicRate;
  }

 doStuff(){
   if (this.PayPeriod=='Monthly') {
    this.Period = 12;
   }
   if (this.PayPeriod=='Quarterly') {
    this.Period = 4;
   }
   if (this.PayPeriod=='Semiannual') {
    this.Period = 2;
   }
   if (this.PayPeriod=='Annual') {
    this.Period = 1;
   }
   this.NumPayment = this.YearsPay * this.Period;
   this.PeriodicRate = ((this.Rate/100) / this.Period);
   this.LoanPayment = this.calcLoanPayment(this.PeriodicRate, this.NumPayment, this.Amount );
   this.Total = this.LoanPayment * this.NumPayment;
   this.Interest = this.Total - this.Amount;
   this.storage.set('LoanPayment', this.LoanPayment);
   this.storage.set('NumPayment', this.NumPayment);
   this.storage.set('Interest', this.Interest);
   this.storage.set('Total', this.Total);
 }
}
