import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {PaymentInfo} from '../paymentinfo/paymentinfo';
import { LumpSum } from '../lumpsum/lumpsum';
import { Affordability} from '../affordability/affordability';
import { FutureValue } from '../futurevalue/futurevalue';
import { PresentValue } from '../presentvalue/presentvalue';
import { LoanPage } from '../loan/loan';
import {SettingsPage } from '../settings/settings';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private storage: Storage) {

  }
  public StateList: any[];
  public ProductList: any[];
  public CalcList: any[];

  ionViewDidLoad() {
  }
  ionViewDidEnter(){
    this.storage.get('StateList').then(data =>{this.StateList = data;})
    this.storage.get('ProductList').then(data =>{this.ProductList = data;})
    this.storage.get('CalcList').then(data =>{this.CalcList = data;})
  }

  ionViewWillEnter(){
  }
  ionViewCanLeave(){
  }

  payFunction(){
    if ((this.StateList && this.ProductList && this.CalcList) == undefined){
          alert('Please Select at least one product, face calculation option, and state by selecting the Settings button above')
    }
    else if ((this.StateList && this.ProductList && this.CalcList) !== undefined) {
      this.navCtrl.push(PaymentInfo)
    }
  }

  lumpFunction(){
    if ((this.StateList && this.ProductList && this.CalcList) == undefined){
          alert('Please Select at least one product, face calculation option, and state by selecting the Settings button above')
    }
    else if ((this.StateList && this.ProductList && this.CalcList) !== undefined) {
      this.navCtrl.push(LumpSum)
    }
  }

  affordFunction(){
    if ((this.StateList && this.ProductList && this.CalcList) == undefined){
          alert('Please Select at least one product, face calculation option, and state by selecting the Settings button above')
    }
    else if ((this.StateList && this.ProductList && this.CalcList) !== undefined) {
      this.navCtrl.push(Affordability)
    }  
  }

  futureFunction(){
    this.navCtrl.push(FutureValue)
  }

  presentFunction(){
    this.navCtrl.push(PresentValue)
  }

  loanFuntion(){
    this.navCtrl.push(LoanPage)
  }
  settingsFunction(){
    this.navCtrl.push(SettingsPage)
  }
}
