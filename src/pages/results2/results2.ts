import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the Results2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-results2',
  templateUrl: 'results2.html',
})
export class Results2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  public State: string;
  public InsuredSignedApp: any;
  public Product: any;
  public FaceCalcOption: any;
  public PayPeriod: any;
  public PayMethod:  any;
  public Age: any;
  public PreArrangementAmt: any;
  public TotalAvailable: any;
  public faceSinglePayment: any;
  public premSinglePayment: any;
  public faceThreePayment: any;
  public premThreePayment: any;
  public faceFivePayment: any;
  public premFivePayment: any;
  public faceSevenPayment: any;
  public premSevenPayment: any;
  public faceTenPayment: any;
  public premTenPayment: any;

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
    this.storage.get('State').then(data =>{this.State = data;})
    this.storage.get('Age').then(data =>{this.Age = data;})
    this.storage.get('Product').then(data =>{this.Product= data;})
    this.storage.get('FaceCalculationOption').then(data =>{this.FaceCalcOption= data;})
    this.storage.get('PayPeriod').then(data =>{this.PayPeriod = data;})
    this.storage.get('PayMethod').then(data =>{this.PayMethod = data;})
    this.storage.get('PreArrangementAmt').then(data =>{this.PreArrangementAmt= data;})
    this.storage.get('TotalAvailable').then(data =>{this.TotalAvailable = data;})
  }

  ionViewDidEnter(){
    this.faceSinglePayment = ((this.PreArrangementAmt - this.TotalAvailable)/this.Age);
    this.premSinglePayment = (this.faceSinglePayment - 1);
    this.faceThreePayment = (this.faceSinglePayment*3);
    this.premThreePayment = (this.premSinglePayment *3);
    this.faceFivePayment = (this.faceSinglePayment*5);
    this.premFivePayment = (this.premSinglePayment *5);
    this.faceSevenPayment = (this.faceSinglePayment*7);
    this.premSevenPayment = (this.premSinglePayment *7);
    this.faceTenPayment = (this.faceSinglePayment*10);
    this.premTenPayment = (this.premSinglePayment *10);
  }

}
