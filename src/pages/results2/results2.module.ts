import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Results2Page } from './results2';

@NgModule({
  declarations: [
    Results2Page,
  ],
  imports: [
    IonicPageModule.forChild(Results2Page),
  ],
})
export class Results2PageModule {}
