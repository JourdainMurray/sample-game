import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public State: string;
  public Product: any;
  public Active: boolean;
  public Balance: boolean;
  public Protect: boolean;
  public Connect: boolean;
  public Select: boolean;
  public HSP: boolean;
  public Plan: boolean;
  public Security: boolean;
  public Advantage: boolean;
  public SecurityPlus: boolean;
  public listOfStates: any[];
  public listOfProducts: any[];
  public listOfCalc: any[];
  public activeStateList: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    
  }

  setStates(){
    this.listOfStates = [{stateName: "Alabama", enabled: false}, {stateName: "Alaska", enabled: false}, {stateName: "Arizona", enabled: false}
    , {stateName: "Arkansas", enabled: false}, {stateName: "California", enabled: false}, {stateName: "Colorado", enabled: false}, {stateName: "Connecticut", enabled: false}
    , {stateName: "Delaware", enabled: false}, {stateName: "District of Columbia", enabled: false}, {stateName: "Florida", enabled: false}, {stateName: "Georgia", enabled: false}
    , {stateName: "Hawaii", enabled: false}, {stateName: "Idaho", enabled: false}, {stateName: "Illinois", enabled: false}, {stateName: "Indiana", enabled: false}
    , {stateName: "Iowa", enabled: false}, {stateName: "Kansas", enabled: false}, {stateName: "Kentucky", enabled: false}, {stateName: "Louisiana", enabled: false}
    , {stateName: "Maine", enabled: false}, {stateName: "Maryland", enabled: false}, {stateName: "Massachusetts", enabled: false}, {stateName: "Michigan", enabled: false}
    , {stateName: "Minnesota", enabled: false}, {stateName: "Mississippi", enabled: false}, {stateName: "Missouri", enabled: false}, {stateName: "Montana", enabled: false}
    , {stateName: "Nebraska", enabled: false}, {stateName: "Nevada", enabled: false}, {stateName: "New Hampshire", enabled: false}, {stateName: "New Jersey", enabled: false}
    , {stateName: "New Mexico", enabled: false}, {stateName: "North Carolina", enabled: false}, {stateName: "North Dakota", enabled: false}, {stateName: "Ohio", enabled: false}
    , {stateName: "Oklahoma", enabled: false}, {stateName: "Oregon", enabled: false}, {stateName: "Pennsylvania", enabled: false}, {stateName: "Rhode Island", enabled: false}
    , {stateName: "South Carolina", enabled: false}, {stateName: "South Dakota", enabled: false}, {stateName: "Tennessee", enabled: false}, {stateName: "Texas", enabled: false}
    , {stateName: "Utah", enabled: false}, {stateName: "Vermont", enabled: false}, {stateName: "Virginia", enabled: false}, {stateName: "West Virginia", enabled: false}
    , {stateName: "Wisconsin", enabled: false}, {stateName: "Wyoming", enabled: false}];
  }

  setProducts(){
    this.listOfProducts = [{productName: "Assurance Active", enabled: false}, {productName: "Assurance Balance", enabled: false}, 
    {productName: "Assurance Protect", enabled: false}, {productName: "Assurance Connect", enabled: false}, {productName: "Assurance Select", enabled: false}, 
    {productName: "HSP", enabled: false}, {productName: "VA/MI Plan", enabled: false}];
  }

  setCalcs(){
    this.listOfCalc = [{calcOptions: "Security", enabled: false}, {calcOptions: "Advantage", enabled: false}, {calcOptions: "SecurityPlus", enabled: false}];
  }
  toggled(x: any){
      console.log(this.listOfStates)
      this.storage.set('StateList', this.listOfStates);
      this.storage.set('ProductList',this.listOfProducts);
      this.storage.set("CalcList", this.listOfCalc);   
  }

  ionViewDidLoad() {
    this.storage.get("CalcList").then(data=>{
      if (!!data){
        this.listOfCalc = data;
      }
      else {
        this.setCalcs();
      }
    })
    this.storage.get("ProductList").then(data=>{
      if (!!data){
        this.listOfProducts = data;
      }
      else {
        this.setProducts();
      }
    })
    this.storage.get("StateList").then(data=>{
      console.log(data);
      if(!!data){
        this.listOfStates = data;
      }
      else {
        this.setStates();
      }
    })
  }
  activeState(Name){
      this.activeStateList.push(Name);
      console.log("active State List:", this.activeStateList)
  }
  deactiveState(Name){
    var i = 0;
    while (this.activeStateList[i] !== undefined){
      if (this.activeStateList[i] == Name ){
        this.activeStateList.splice(i, 1)
        }
      }
      i++;
      console.log("active State List:", this.activeStateList)
  }

  ionViewCanLeave(){
 
    this.storage.set('activeStates', this.activeStateList)
  }
 
  doStuff(){
  }
  clearFunction(){
    this.storage.set('StateList', null);
    this.storage.set('ProductList', null);
    this.storage.set('CalcList', null);
    this.storage.set('Age', null);
    this.storage.set('State', null);
    this.storage.set('InsuredSignedApp', null);
    this.storage.set('Product', null);
    this.storage.set('FaceCalculationOption', null);
    this.storage.set('PayPeriod', null);
    this.storage.set('PayMethod', null);
    this.storage.set('PreArrangementAmt', null);
    this.storage.set('DownPaymentAmt',null); 
    this.storage.set('Amount', null);
    this.storage.set('Rate', null);
    this.storage.set('YearsPay', null);
    this.storage.set('TotalAvailable', null);
    this.storage.set('PresentValue', null);
    this.storage.set('FutureValue', null);
    this.storage.set('LoanPayment', null);
    this.storage.set('NumPayment',null);
    this.storage.set('Interest', null);
    this.storage.set('Total', null);
    this.setStates();
    this.setCalcs();
    this.setProducts();
    alert('All Data Fields cleared')
  }
}
