import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';


/**
 * Generated class for the ResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  constructor
  (public navCtrl: NavController, 
    public navParams: NavParams,
     private storage: Storage, 
     private http: Http ) {
  }
 
  public State: string;
  public InsuredSignedApp: any;
  public Product: any;
  public FaceCalcOption: any;
  public PayPeriod: any;
  public PayMethod:  any;
  public Age: any;
  public PreArrangementAmt: any;
  public DownPaymentAmt: any;
  public faceSinglePayment: any;
  public premSinglePayment: any;
  public faceThreePayment: any;
  public premThreePayment: any;
  public faceFivePayment: any;
  public premFivePayment: any;
  public faceSevenPayment: any;
  public premSevenPayment: any;
  public faceTenPayment: any;
  public premTenPayment: any;
  public holder: any[];

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
    this.storage.get('State').then(data =>{this.State = data;})
    this.storage.get('Age').then(data =>{this.Age = data;})
    this.storage.get('Product').then(data =>{this.Product= data;})
    this.storage.get('FaceCalculationOption').then(data =>{this.FaceCalcOption= data;})
    this.storage.get('PayPeriod').then(data =>{this.PayPeriod = data;})
    this.storage.get('PayMethod').then(data =>{this.PayMethod = data;})
    this.storage.get('PreArrangementAmt').then(data =>{this.PreArrangementAmt= data;})
    this.storage.get('DownPaymentAmt').then(data =>{this.DownPaymentAmt = data;})
  }

  ionViewDidEnter(){
    //this.loadItems();
    this.login();

    // this.faceSinglePayment = ((this.PreArrangementAmt - this.DownPaymentAmt)/this.Age);
    // this.premSinglePayment = (this.faceSinglePayment - 1);
    // this.faceThreePayment = (this.faceSinglePayment*3);
    // this.premThreePayment = (this.premSinglePayment *3);
    // this.faceFivePayment = (this.faceSinglePayment*5);
    // this.premFivePayment = (this.premSinglePayment *5);
    // this.faceSevenPayment = (this.faceSinglePayment*7);
    // this.premSevenPayment = (this.premSinglePayment *7);
    // this.faceTenPayment = (this.faceSinglePayment*10);
    // this.premTenPayment = (this.premSinglePayment *10);
    
  }

  theCalculations(){  
  }

  doStuff(){
    // this.http.post("http://test.myhomesteaders.com/UIS/jserv.asmx?op=PaymentInformationCalculation", { PersonAge: this.Age, TheState: this.State, 
    // ISA: this.InsuredSignedApp, TheProduct: this.Product, PayPer: this.PayPeriod, PayMeth: this.PayMethod, PreArrange: this.PreArrangementAmt, DownPaym: this.DownPaymentAmt })
    //               .toPromise()
    //               .then((data) => { this.holder; })

    // this.faceSinglePayment = this.holder[1];
    // this.premSinglePayment = this.holder[2];
    // this.faceThreePayment = this.holder[3];
    // this.premThreePayment = this.holder[4];
    // this.faceFivePayment = this.holder[5];
    // this.premFivePayment = this.holder[6];
    // this.faceSevenPayment = this.holder[7];
    // this.premSevenPayment = this.holder[8];
    // this.faceTenPayment = this.holder[9];
    // this.premTenPayment = this.holder[10];
  }


  loadItems(){


  }

  tester(){
    this.http.post("http://dev.myhomesteaders.com/OEV2/JServ.asmx/getOEPending", {}, {withCredentials: true}).subscribe(data => {
   
      var itemList = JSON.parse(JSON.parse(data["_body"]).d);
      itemList.forEach(element => {
        console.log()
      });
   
    },
    err=>{console.log(err)})

  }


  
  login() {
    return new Promise((resolve, reject) => {
      this.http.get("https://test.myhomesteaders.com/UIS/native.asmx/login?username=5J006&password=tester1", { withCredentials: true })
        .toPromise()
        .then((data) => {
          console.log(data);
          console.log(data["_body"]);
          console.log("Logged INNNNNNN")
      
        })
        .catch((error) => {
          alert("Unable to connect to the internet. Please check your connection and try again.");
        })
    })
  }


}
