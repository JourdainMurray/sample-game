import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage'


@Component({
  selector: 'page-futurevalue',
  templateUrl: 'futurevalue.html'
})
export class FutureValue {
  public Amount: number;
  public Rate: number;
  public YearsPay: number;
  public FutureValue: number;
  public Count: number;

  public stateList: string[];

  constructor(public navCtrl: NavController, private storage: Storage) {
  }

  ionViewDidLoad(){
    
  }
  
  ionViewDidEnter(){
    this.storage.get('Amount').then(data =>{this.Amount = data;})
    this.storage.get('Rate').then(data =>{this.Rate = data;})
    this.storage.get('YearsPay').then(data =>{this.YearsPay = data;})
    this.storage.get('FutureValue').then(data =>{this.FutureValue = data})
  }

  ionViewCanLeave(){
    this.storage.set('Amount', this.Amount);
    this.storage.set('Rate', this.Rate);
    this.storage.set('YearsPay', this.YearsPay);
  }
  
  doStuff(){
    this.FutureValue = this.Amount * ((1 + (this.Rate/100)) ** this.YearsPay);
    /*
    this.Count = 0;
    this.FutureValue = this.Amount;
        while (this.YearsPay > this.Count){
          this.FutureValue = this.FutureValue*((this.Rate/100) + 1);
      this.Count++;
      }
    this.storage.set('FutureValue', this.FutureValue);
      */
    }
}