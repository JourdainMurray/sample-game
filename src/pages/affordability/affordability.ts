import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage'
import { Results3Page } from '../results3/results3'

@Component({
  selector: 'page-Affordability',
  templateUrl: 'Affordability.html'
})
export class Affordability {
  constructor(public navCtrl: NavController, private storage: Storage) {
 }

 public State: any;
 public InsuredSignedApp: string;
 public Product: string;
 public FaceCalculationOption: string;
 public PayPeriod: string;
 public PayMethod: string;
 public Age: any;
 public Premium: any;
 public DownPaymentAmt: any;
 public StateList: any[];
 public ProductList: any[];
 public CalcList: any[];

  ionViewDidEnter() {
    this.storage.get('StateList').then(data=>{this.StateList = data;})
    this.storage.get("ProductList").then(data=>{this.ProductList = data;})
    this.storage.get("CalcList").then(data=>{this.CalcList = data;})
    this.storage.get('Age').then(data =>{this.Age = data;})
    this.storage.get('State').then(data =>{this.State = data;})
    this.storage.get('InsuredSignedApp').then(data =>{this.InsuredSignedApp = data;})
    this.storage.get('Product').then(data =>{this.Product = data;})
    this.storage.get('FaceCalculationOption').then(data =>{this.FaceCalculationOption = data;})
    this.storage.get('PayPeriod').then(data =>{this.PayPeriod = data;})
    this.storage.get('PayMethod').then(data =>{this.PayMethod = data;})
    this.storage.get('Premium').then(data =>{this.Premium= data;})
    this.storage.get('DownPaymentAmt').then(data =>{this.DownPaymentAmt = data;})
  }

  ionViewCanLeave(){
    this.storage.set('Age', this.Age);
    this.storage.set('State', this.State);
    this.storage.set('InsuredSignedApp', this.InsuredSignedApp);
    this.storage.set('Product', this.Product);
    this.storage.set('FaceCalculationOption', this.FaceCalculationOption);
    this.storage.set('PayPeriod', this.PayPeriod);
    this.storage.set('PayMethod', this.PayMethod);
    this.storage.set('Premium', this.Premium);
    this.storage.set('DownPaymentAmt', this.DownPaymentAmt);
  }
  calcFunction(){
    if (this.Age == undefined){
      alert('age is undefined, please enter age')
    }
    if (this.State == undefined){
      alert('State is undefined, please enter State')
    }
    if (this.InsuredSignedApp == undefined){
      alert('Insured Signed App is undefined, please enter a selection')
    }
    if (this.Product == undefined){
      alert('Product is undefined, please designate a product')
    }
    if (this.FaceCalculationOption == undefined){
      alert('Face Calculation Option is undefined, please enter a valid option')
    }
    if (this.PayPeriod == undefined){
      alert('Pay Period is undefined, please enter a proper Pay Period')
    }
    if (this.PayMethod == undefined){
      alert('Method of payment is undefined, please enter a proper Method')
    }
    if (this.Premium == undefined){
      alert('Premium amount is undefined, please enter an amount')
    }
    if (this.DownPaymentAmt == undefined){
      alert('Down Payment amount is undefined, please enter an amount')
    }
    if ((this.Age && this.State && this.InsuredSignedApp && this.Product && this.FaceCalculationOption && this.PayPeriod && this.PayMethod && 
          this.Premium &&this.DownPaymentAmt) !==  null){
            this.navCtrl.push(Results3Page)
          }
  }

  clearFunction(){
    this.Age = null;
    this.State = null;
    this.InsuredSignedApp = null;
    this.Product = null;
    this.FaceCalculationOption = null;
    this.PayPeriod = null;
    this.PayMethod = null;
    this.Premium = null;
    this.DownPaymentAmt = null;
    alert('All Data Fields cleared')
  }
}