import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Results3Page } from './results3';

@NgModule({
  declarations: [
    Results3Page,
  ],
  imports: [
    IonicPageModule.forChild(Results3Page),
  ],
})
export class Results3PageModule {}
