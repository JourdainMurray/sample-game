import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresentValue } from './presentvalue';

@NgModule({
  declarations: [
    PresentValue,
  ],
  imports: [
    IonicPageModule.forChild(PresentValue),
  ],
})
export class PresentvaluePageModule {}
