import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage'

/**
 * Generated class for the PresentvaluePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-presentvalue',
  templateUrl: 'presentvalue.html',
})
export class PresentValue {
  public Amount: number;
  public Rate: number;
  public YearsPay: number;
  public PresentValue: number;
  public Count: number;

  constructor(public navCtrl: NavController, private storage: Storage) {
  }

  ionViewDidLoad(){
  }

  ionViewDidEnter(){
    this.storage.get('Amount').then(data =>{this.Amount = data;})
    this.storage.get('Rate').then(data =>{this.Rate = data;})
    this.storage.get('YearsPay').then(data =>{this.YearsPay = data;})
    this.storage.get('PresentValue').then(data =>{this.PresentValue = data})
  }

  ionViewCanLeave(){
    this.storage.set('Amount', this.Amount);
    this.storage.set('Rate', this.Rate);
    this.storage.set('YearsPay', this.YearsPay);
  }

  doStuff(){
    this.PresentValue = this.Amount / ((1 + (this.Rate/100)) ** this.YearsPay);
    this.storage.set('PresentValue', this.PresentValue);
  }

}
